{-# LANGUAGE RecordWildCards #-}

module Mushroom.Typechecker.Util
  ( typeError
  , addVar
  , addTypeVar
  , lookupVar
  , lookupType
  , addTag
  , lookupTag
  , enterModule
  , exitModule
  , addPrefix
  , pushCtx
  , popCtx
  , withTempTermCtx
  , withTempTypeCtx
  , withTempTagCtx
  , freshTypeVar ) where

import Numeric (showHex)

import Control.Monad.Except
import Control.Monad.State
import Data.List (intercalate)

import Control.Lens
import qualified Data.Set as S
import qualified Data.Map.Strict as M

import Mushroom.Types

-- | Add a variable to the context.
addVar :: Symbol -> Type -> MushroomTypecheck ()
addVar sym ty = vars %= \v -> v & ix 0 %~ M.insert sym ty

-- | Add a type variable to the context.
addTypeVar :: Symbol -> Type -> MushroomTypecheck ()
addTypeVar sym ty = typeVars %= \v -> v & ix 0 %~ M.insert sym ty          

-- | Adds a tag and a type to the tag map.
addTag :: Symbol -> Type -> MushroomTypecheck ()
addTag sym ty = tagMap %= \v -> v & ix 0 %~ M.insert sym ty

-- | Prefixes elements of a type that need to be prefixed.
prefixTy :: Type -> MushroomTypecheck Type
prefixTy (Type'Variant name tags) = do
  name' <- addPrefix name
  tags' <- M.fromList `fmap`
           mapM (\(k, v) -> addPrefix k >>= \k' -> return (k', v))
           (M.toList tags)
  return (Type'Variant name' tags')
prefixTy (Type'TypeAbs ps ret) = prefixTy ret >>= \ret' -> return (Type'TypeAbs ps ret')
prefixTy ty = return ty

typeError :: String -> MushroomTypecheck a
typeError = throwError . newMushroomError TypeError

-- | Look up a variable's type via name.
lookupVar :: Symbol -> MushroomTypecheck Type
lookupVar sym = do
  ctx <- uses vars head
  let ty = ctx M.!? sym
  case ty of
    Just ty' -> return ty'
    Nothing -> typeError ("Undefined variable " ++ sym)

-- | If given a type variable, it returns the type that the variable aliases.
-- otherwise, the type is just returned unchanged.
lookupType :: Type -> MushroomTypecheck Type
lookupType (Type'TypeVar sym) = do
  ty <- findType sym
  case ty of
    Just ty' -> lookupType ty' -- keep searching -- @ty'@ could be another variable
    Nothing -> typeError ("Undefined type variable " ++ sym)
lookupType ty = return ty

-- | Looks up a variant tag to see what type (if any) it's a member of.
lookupTag :: Symbol -> MushroomTypecheck Type
lookupTag sym = do
  ty <- uses tagMap ((M.!? sym) . head)
  case ty of
    Just var -> return var
    Nothing -> typeError ("Unknown variant tag `" ++ sym ++ "'.")

-- | Look up a type variable by its symbol, returning a @Maybe Type@ value.
findType :: Symbol -> MushroomTypecheck (Maybe Type)
findType sym = uses typeVars ((M.!? sym) . head)

-- | 'Enter' a module by recording its name.
enterModule :: Symbol -> MushroomTypecheck ()
enterModule modName = moduleNames %= (modName:)

-- | 'Exit' a module by setting the module name to nothing.
exitModule :: MushroomTypecheck ()
exitModule = moduleNames %= tail

-- | Prefix an identifier with all the current module names.
addPrefix :: Symbol -> MushroomTypecheck Symbol
addPrefix ('#':xs) = ('#':) `fmap` addPrefix xs
addPrefix ('%':xs) = ('%':) `fmap` addPrefix xs
addPrefix ident = do
  prefix <- uses moduleNames (\xs -> if null xs then "" else head xs ++ "::")
  return (prefix ++ ident)

-- | Given an arrow to access either the first or second type map,
-- push a new version of that context (extended with @newCtx@) to the context stack.
pushCtx l newCtx = l %= push
  where push xs = (head xs `withCtx` newCtx):xs

-- | Remove the top context from the context stack referred to by @arr@.
-- Both parameters to this function should refer to the same lens. The reason that they need to be
-- repeated is due to some strange getter/setter polymorphism issue with lenses.
popCtx l l' = do
  topCtx <- uses l' head
  l %= tail
  return topCtx

-- | Perform an action @f@ with a temporary extension (@ctx@) to the context specified by @arr@.
withTempCtx l l' f ctx = do
  pushCtx l ctx
  result <- f
  popCtx l l'
  return result

-- | Perform an action with an extension to the context mapping variables to types.
withTempTermCtx :: MushroomTypecheck a -> TypeMap -> MushroomTypecheck a
withTempTermCtx = withTempCtx vars vars

-- | Perform an action with an extension to the context mapping type variables to types.
withTempTypeCtx :: MushroomTypecheck a -> TypeMap -> MushroomTypecheck a
withTempTypeCtx = withTempCtx typeVars typeVars

-- | Perform an action with an extension to the variant tag map.
withTempTagCtx :: MushroomTypecheck a -> TypeMap -> MushroomTypecheck a
withTempTagCtx = withTempCtx tagMap tagMap

-- | Creates a fresh (unique) type variable
freshTypeVar :: MushroomTypecheck Type
freshTypeVar = do
  n <- use typeVarCount
  let sym = "'x" ++ showHex n ""
  typeVarCount += 1 -- don't worry, this is still Haskell
  return (Type'Placeholder sym)
