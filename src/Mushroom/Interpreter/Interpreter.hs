module Mushroom.Interpreter.Interpreter (eval, runInterpreter) where

import Control.Arrow (first)
import Control.Lens
import Control.Monad (foldM)
import Control.Monad.Except
import Control.Monad.IO.Class
import Control.Monad.State

import Data.Fix
import Data.List
import Data.Maybe (fromJust)
import Data.Text (pack, unpack)
import Data.Tuple (swap)
import qualified Data.Map.Strict as M

import Debug.Trace (trace)

import Mushroom.Interpreter.Util
import Mushroom.PrettyPrint
import Mushroom.Types

-- | Evaluate a list of successive @Terms@ provided the @InterpreterState@, yielding an error if
-- something goes wrong, and otherwise returning updated @InterpreterState@ and a list of the
-- evaluated @Terms@.
runInterpreter :: InterpreterState -> [TypedTerm] ->
  IO (Either MushroomError (InterpreterState, [TypedTerm]))
runInterpreter inState terms = results >>= \r ->
  case sequence $ snd r of
    Left msg -> return $ Left msg
    Right terms' -> return $ Right (fst r, terms')
  where results = mapAccumLM go inState terms
        go b t = swap `fmap` runStateT (runExceptT . runEval $ eval t) b

eval :: TypedTerm -> MushroomEval TypedTerm
-- for definitions, evaluate the variable's value and save it
eval Fix{unFix = Term'Define sym term `Typed` _} = do
  -- evaluate and record the term
  val <- eval term
  addVar sym val
  return Fix{unFix = Term'None `Typed` Type'None}

eval Fix{unFix = Term'ModuleDecl modName termExports _ defs `Typed` _} = do
  -- create a new context for bindings to capture the module's variables
  pushBindings M.empty
  -- evaluate the definitions and do nothing with the results
  mapM_ eval defs
  -- collect the variables defined in the module
  modVars <- M.toList `fmap` popBindings
  -- filter out the public variables and then prefix them with qualifiers
  let modVars' = exportCtx modVars
  mapM_ (uncurry addVar) modVars'
  return Fix{unFix = Term'None `Typed` Type'None}
  where exportCtx = prefixExports . filterExports
        prefixExports = map (first (\k -> modName ++ "::" ++ k))
        filterExports = filter (\(k, v) -> k `elem` termExports)

-- For record declarations, create projection functions for each field.
-- Record declarations essentially just "write" even more code when they're evaluated, so it gets
-- kind of messy.
eval Fix{unFix = Term'RcdDecl sym fields `Typed` _} = do
  -- reconstruct the type of the record, because we'll need it when constructing the projections
  let rcdType = Type'Record sym (M.fromList fields)
  -- make a projection function for every field
  forM_ fields $ \(field, _) -> do
    -- the projection function matches on the record, binds the desired field
    -- to "$x", and then returns that field
    let fieldType = fromJust (lookup field fields)
        mkMatcher (sym, ty) = let binding = if sym == field then "$x" else "$_"
                              in (sym, Fix $ Term'Var binding `Typed` ty)
        fieldMatchers = M.fromList (map mkMatcher fields)
        matcher = Fix (Term'Record sym fieldMatchers `Typed` rcdType)
        matchResult = Fix (Term'Var "$x" `Typed` fieldType)
        proj = Fix { unFix = Term'Abs ["rec"] M.empty
                             [ Fix { unFix = Term'Match (Fix $ Term'Var "rec" `Typed` rcdType)
                                    [(matcher, [matchResult])]
                                    `Typed` fieldType } ]
                     `Typed` Type'Abs [rcdType] fieldType }
    addVar field proj
  return Fix{unFix = Term'None `Typed` Type'None}

-- look up and evaluate variables
eval Fix{unFix = Term'Var sym `Typed` _} = do
  val <- uses bindings ((M.!? sym) . head)
  case val of
    Just val' -> eval val'
    -- should be reported by typechecker already
    Nothing -> runtimeError ("Undefined variable " ++ sym)

-- when evaluating abstractions, save their current environment
eval Fix{unFix = Term'Abs params _ body `Typed` ty} = uses bindings head >>= \env ->
  return Fix{unFix = Term'Abs params env body `Typed` ty}

-- for applications, handle the cases for builtins, abstractions, and other values
eval Fix{unFix = Term'AppAbs abs args `Typed` ty} = do
  abs' <- eval abs
  args' <- mapM eval args
  case abs' of
    Fix{unFix = Term'Builtin name `Typed` _} -> handleBuiltin name ty args'
    Fix{unFix = Term'Abs params env body `Typed` _} -> do
      let argBindings = M.fromList $ zip params args'
      results <- mapM eval body `withTempBindings` (env `withCtx` argBindings)
      return (last results)
    t -> runtimeError ("Expected abstraction, got " ++ prettyPrint t)

-- evaluate all members of a product
eval Fix{unFix = Term'Product terms `Typed` ty} = do
  terms' <- mapM eval terms
  return Fix{unFix = Term'Product terms' `Typed` ty}

-- for matches, find and evaluate the first successful branch
eval Fix{unFix = Term'Match matchee cases `Typed` _} = do
  matchee' <- eval matchee
  matches <- filterM (matchPattern matchee') (map fst cases)
  -- find first pattern that matches
  case matches of
    [] -> runtimeError "Non-exhaustive patterns in match"
    -- using @fromJust@ is safe since we know @key@ exists
    (key:_) -> let result = fromJust (lookup key cases)
               in matchBindings matchee' key >>=
                  \bindings -> last `fmap` mapM eval result `withTempBindings` bindings

-- evaluate variant params
eval Fix{unFix = Term'Variant tag params `Typed` ty} =
  mapM eval params >>= \params' -> return Fix{unFix = Term'Variant tag params' `Typed` ty}

-- evaluate record fields
eval Fix{unFix = Term'Record name fields `Typed` ty} =
  mapM eval fields >>= \fields' -> return Fix{unFix = Term'Record name fields' `Typed` ty}

-- evaluate all members of a list
eval Fix{unFix = Term'List terms `Typed` ty} = do
  terms' <- mapM eval terms
  return Fix{unFix = Term'List terms' `Typed` ty}

-- evaluate all members of a set, and then remove the duplicates
eval Fix{unFix = Term'Set terms `Typed` ty} = do
  terms' <- mapM eval terms
  let nubTerms = sort (nub terms') -- remove duplicates and sort remaining elements
  return Fix{unFix = Term'Set nubTerms `Typed` ty}

-- do nothing for variant declarations except return @Term'None@ since they can't be used as values
eval Fix{unFix = Term'VarDecl _ _ `Typed` _} = return Fix{unFix = Term'None `Typed` Type'None}

-- just like for abstractions, save the current environment when evaluating a fiber
eval Fix{unFix = Term'Fiber params env body `Typed` ty} = uses bindings head >>= \env ->
  return Fix{unFix = Term'Fiber params env body `Typed` ty}

-- Resume statements are similar to applying an abstraction, but the body of the fiber is 'consumed'
-- until after a yield statement is encountered.
eval Fix{unFix = Term'Resume fiberName args `Typed` ty} = do
  fiber <- eval Fix{unFix = Term'Var fiberName `Typed` Type'None}
  args' <- mapM eval args
  case fiber of
    Fix{unFix = Term'Fiber params env body `Typed` fiberTy} -> do
      let argBindings = M.fromList $ zip params args'
          -- take the start of the body up to the yield, save the yield, and save the rest
          (bodyStart, (yieldStmt:bodyRest)) = break isYield body
      -- Push the new bindings onto the stack. It's important that we use @pushBindings@ and
      -- @popBindings@ instead of just @withTempBindings@ so that we can store the new variable
      -- declarations back into the fiber when we're done evaluating.
      pushBindings (env `withCtx` argBindings)
      mapM eval bodyStart
      yieldedVal <- eval yieldStmt
      env' <- popBindings
      -- Associate the updated fiber back with the fiber's name. This fiber's state may now have
      -- (a) additional local declarations in the variable bindings; and/or
      -- (b) a reduced number of terms in the body.
      -- Also, if the body doesn't have any statements except for the yield statement left, then just
      -- leave that yield statement there for whatever future resume calls will be made.
      let body' = if null bodyRest then [yieldStmt] else bodyRest
          newFiber = Fix{unFix = Term'Fiber params env' body' `Typed` fiberTy}
      addVar fiberName newFiber
      -- return the value that was yielded
      return yieldedVal
    _ -> runtimeError "Expected a fiber in resume statement"
  where isYield Fix{unFix = Term'Yield _ `Typed` _} = True
        isYield _ = False
  
eval Fix{unFix = Term'Yield val `Typed` ty} = eval val

-- for other terms, do nothing to them
eval term = return term

-- | Apply a builtin function to arguments.
-- The arguments should already be evaluated for this to work.
-- This function takes the overall type of the application as the first argument.
handleBuiltin :: Builtin -> Type -> [TypedTerm] -> MushroomEval TypedTerm
handleBuiltin Builtin'Add _ [Fix (Term'IntLit x `Typed` _), Fix (Term'IntLit y `Typed` _)] =
  return Fix{unFix = Term'IntLit (x + y) `Typed` Type'Int}
handleBuiltin Builtin'Sub _ [Fix (Term'IntLit x `Typed` _), Fix (Term'IntLit y `Typed` _)] =
  return Fix{unFix = Term'IntLit (x - y) `Typed` Type'Int}
handleBuiltin Builtin'Div _ [Fix (Term'IntLit x `Typed` _), Fix (Term'IntLit 0 `Typed` _)] =
  runtimeError "Cannot divide by 0"
handleBuiltin Builtin'Div _ [Fix (Term'IntLit x `Typed` _), Fix (Term'IntLit y `Typed` _)] =
  return Fix{unFix = Term'IntLit (x `div` y) `Typed` Type'Int}
handleBuiltin Builtin'Mul _ [Fix (Term'IntLit x `Typed` _), Fix (Term'IntLit y `Typed` _)] =
  return Fix{unFix = Term'IntLit (x * y) `Typed` Type'Int}
handleBuiltin Builtin'Prn _ [Fix (Term'StrLit str `Typed` _)] =
  liftIO (putStr (unpack str)) >> return Fix{unFix = Term'Product [] `Typed` Type'Product []}
handleBuiltin Builtin'Hd _ [Fix (Term'List [] `Typed` _)] =
  runtimeError "Cannot take head of empty list"
handleBuiltin Builtin'Hd _ [Fix (Term'List (x:xs) `Typed` Type'List _)] =
  return x
handleBuiltin Builtin'Tl _ [Fix (Term'List [] `Typed` _)] =
  runtimeError "Cannot take tail of empty list"
handleBuiltin Builtin'Tl ty [Fix (Term'List (x:xs) `Typed` _)] =
  return Fix{unFix = Term'List xs `Typed` ty}
handleBuiltin Builtin'Cons ty [x, Fix (Term'List xs `Typed` _)] =
  return Fix{unFix = Term'List (x:xs) `Typed` ty}
handleBuiltin Builtin'StrToList _ [Fix (Term'StrLit xs `Typed` Type'Str)] =
  let xs' = map (\c -> Fix (Term'CharLit c `Typed` Type'Char)) (unpack xs)
  in return Fix{unFix = Term'List xs' `Typed` Type'List Type'Char}
handleBuiltin Builtin'ListToStr _ [Fix (Term'List xs `Typed` _)] =
  let xs' = pack $ map (\(Fix (Term'CharLit c `Typed` Type'Char)) -> c) xs
  in return Fix{unFix = Term'StrLit xs' `Typed` Type'Str}

-- | Return a boolean describing whether the given pattern matches the given value.
matchPattern :: TypedTerm -> TypedTerm -> MushroomEval Bool
matchPattern _ (Fix (Term'Var _ `Typed` _)) = return True
matchPattern (Fix (Term'IntLit n1 `Typed` _)) (Fix (Term'IntLit n2 `Typed` _)) = return (n1 == n2)
matchPattern (Fix (Term'StrLit s1 `Typed` _)) (Fix (Term'StrLit s2 `Typed` _)) = return (s1 == s2)
matchPattern (Fix (Term'Product ts1 `Typed` _)) (Fix (Term'Product ts2 `Typed` _)) = do
  elemsMatch <- all (== True) `fmap` zipWithM matchPattern ts1 ts2
  return $ length ts1 == length ts2 && elemsMatch
matchPattern (Fix (Term'Builtin s1 `Typed` _)) (Fix (Term'Builtin s2 `Typed` _)) = return (s1 == s2)
matchPattern (Fix (Term'List ts1 `Typed` _)) (Fix (Term'List ts2 `Typed` _)) = do
    elemsMatch <- all (== True) `fmap` zipWithM matchPattern ts1 ts2
    return $ length ts1 == length ts2 && elemsMatch
matchPattern (Fix (Term'Set ts1 `Typed` _)) (Fix (Term'Set ts2 `Typed` _)) = do
    elemsMatch <- all (== True) `fmap` zipWithM matchPattern ts1 ts2
    return $ length ts1 == length ts2 && elemsMatch
matchPattern (Fix (Term'Variant t1 a1 `Typed` _)) (Fix (Term'Variant t2 a2 `Typed` _)) = do
    elemsMatch <- all (== True) `fmap` zipWithM matchPattern a1 a2
    return $ t1 == t2 && elemsMatch
matchPattern (Fix (t2 `Typed` _)) (Fix (Term'Pin t1 `Typed` _)) = do
  (Fix (t1' `Typed` _)) <- eval t1
  return (t1' == t2)
matchPattern (Fix (Term'Record n1 m1 `Typed` _)) (Fix (Term'Record n2 m2 `Typed` _)) = do
  elemsMatch <- all (== True) `fmap` zipWithM matchPattern (M.elems m1) (M.elems m2)
  return (n1 == n2 && elemsMatch)
matchPattern (Fix (x `Typed` _)) (Fix (y `Typed` _)) = return (x == y)

-- | Create variable bindings from a pattern that matches a value.
matchBindings :: TypedTerm -> TypedTerm -> MushroomEval (M.Map Symbol TypedTerm)
matchBindings t (Fix (Term'Var sym `Typed` _)) = return (M.singleton sym t)
matchBindings (Fix (Term'Product ts1 `Typed` _)) (Fix (Term'Product ts2 `Typed` _)) = do
  bindings <- zipWithM matchBindings ts1 ts2
  return (mconcat bindings)
matchBindings (Fix (Term'List ts1 `Typed` _)) (Fix (Term'List ts2 `Typed` _)) = do
  bindings <- zipWithM matchBindings ts1 ts2
  return (mconcat bindings)
matchBindings (Fix (Term'Set ts1 `Typed` _)) (Fix (Term'Set ts2 `Typed` _)) = do
  bindings <- zipWithM matchBindings ts1 ts2
  return (mconcat bindings)
matchBindings (Fix (Term'Variant _ a1 `Typed` _)) (Fix (Term'Variant _ a2 `Typed` _)) = do
  bindings <- zipWithM matchBindings a1 a2
  return (mconcat bindings)
matchBindings (Fix (Term'Record _ m1 `Typed` _)) (Fix (Term'Record _ m2 `Typed` _)) = do
  bindings <- zipWithM matchBindings (M.elems m1) (M.elems m2)
  return (mconcat bindings)
matchBindings _ _ = return M.empty

-- | This is a function from some internal module that for some reason can't be imported, so I just
-- copied it verbatim from stackage. All credit goes to the wizard who actually devised this function.
mapAccumLM :: Monad m => (acc -> x -> m (acc, y)) -> acc -> [x] -> m (acc, [y])
mapAccumLM _ s [] = return (s, [])
mapAccumLM f s (x:xs) = do
  (s1, x') <- f s x
  (s2, xs') <- mapAccumLM f s1 xs
  return (s2, x':xs')
