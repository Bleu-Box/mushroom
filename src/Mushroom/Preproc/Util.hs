{-# LANGUAGE LambdaCase #-}

module Mushroom.Preproc.Util where

import Control.Lens
import Control.Monad
import Control.Monad.Except

import Data.Fix
import qualified Data.Map.Strict as M
import Data.Maybe

import Debug.Trace (trace)

import Mushroom.PrettyPrint
import Mushroom.Types

-- | Add a macro by name to the list of macros.
addMacro :: Symbol -> MacroInfo -> MushroomPreproc ()
addMacro name mac = macros %= M.insert name mac

-- | Determines whether a term is a macro declaration.
isMacroDecl :: Term -> Bool
isMacroDecl Fix{unFix = Term'MacroDecl _ _ _ _} = True
isMacroDecl _                                   = False

-- | Looks up a macro's information by name, yielding an error if the macro cannot be found.
lookupMacro :: Symbol -> MushroomPreproc MacroInfo
lookupMacro name = do
  minfo <- uses macros (M.!? name)
  case minfo of
    Just info -> return info
    Nothing -> preprocError ("Cannot find macro `" ++ name ++ "'")

-- | Typechecks macro arguments.
checkArgs :: [SyntaxElement] -> [(Symbol, SyntaxType)] -> MushroomPreproc ()
checkArgs args ps =
  let errors = concat $ zipWith typecheckSyntax args (map snd ps)
  in forM_ errors $ \case
    Nothing -> return ()
    Just (elt, ty) -> preprocError ("Syntax element `" ++ prettyPrint elt
                                    ++ "' does not have type `" ++ prettyPrint ty ++ "'")

-- | Compares a @SyntaxElement@ to a @SyntaxType@. It returns @Nothing@ if the element is a
-- member of that type, and returns the two arguments otherwise for error reporting purposes.
typecheckSyntax :: SyntaxElement -> SyntaxType -> [Maybe (SyntaxElement, SyntaxType)]
typecheckSyntax a@(Syntax'Name _) b@SyntaxTy'Term = [Just (a, b)]
typecheckSyntax a@(Syntax'List _) b@SyntaxTy'Term = [Just (a, b)]
typecheckSyntax _ SyntaxTy'Term = [Nothing]
typecheckSyntax (Syntax'Decl _) SyntaxTy'Decl = [Nothing]
typecheckSyntax (Syntax'Val _) SyntaxTy'Val = [Nothing]
typecheckSyntax (Syntax'Type _) SyntaxTy'Type = [Nothing]
typecheckSyntax (Syntax'Name _) SyntaxTy'Name = [Nothing]
typecheckSyntax (Syntax'List ts) (SyntaxTy'List ty) = ts >>= (\t -> typecheckSyntax t ty)
typecheckSyntax a b = [Just (a, b)]

-- | "Applies" a macro by replacing the arguments with the parameters.
applyMacro :: MacroInfo -> [SyntaxElement] -> Maybe SyntaxElement -> MushroomPreproc [Term]
applyMacro (ps1, ps2, body) args1 args2 = undefined -- concat `fmap` mapM app body
  -- where args1' = zip (map fst ps1) args1
  --       -- TODO:
  --       -- + incorporate args2 into this
  --       -- + try abstracting out all the repetition (maybe w a traversal fn?)
  --       app term = case term of
  --         Fix (Term'Var name) -> do
  --           -- a @Term'Var@ that has a macro splice can be multiple things:
  --           -- + a name splice, which evaluates to a symbol
  --           -- + a @Term@ splice, which would need to be pulled out of its @Syntax@ wrapper
  --           -- this case handles both options, and throws an error if neither ends up
  --           -- working (i.e. the splice isn't an (existing) word/symbol)
  --           name' <- (Just `fmap` evalSym args1' name) `catchError` const (return Nothing)
  --           case name' of
  --             Just sym -> return [Fix (Term'Var sym)]
  --             -- if it's not a valid symbol splice, it must be a term splice
  --             Nothing -> app (Fix (Term'MacroSplice (tail name)))
  --         Fix (Term'Pin t) -> do
  --           t' <- (app >=> extractSingleton) t
  --           return [Fix (Term'Pin t')]
  --         Fix (Term'MacroDecl name p1 p2 body) -> do
  --           name' <- evalSym args1' name
  --           body' <- concat `fmap` mapM app body
  --           return [Fix (Term'MacroDecl name' p1 p2 body')]
  --         Fix (Term'Define name t) -> do
  --           name' <- evalSym args1' name
  --           t' <- (app >=> extractSingleton) t
  --           return [Fix (Term'Define name' t')]
  --         Fix (Term'ModuleDecl name ts tys body) -> undefined
  --         --- do these once you figure out how to splice types --
  --         Fix (Term'VarDecl name tags) -> undefined
  --         Fix (Term'RcdDecl name fields) -> undefined
  --         -------------------------------------------------------
  --         Fix (Term'List ts) -> do
  --           ts' <- concat `fmap` mapM app ts
  --           return [Fix (Term'List ts')]
  --         Fix (Term'Product ts) -> do
  --           ts' <- concat `fmap` mapM app ts
  --           return [Fix (Term'Product ts')]
  --         Fix (Term'Variant name ts) -> do
  --           name' <- evalSym args1' name
  --           ts' <- concat `fmap` mapM app ts
  --           return [Fix (Term'Variant name' ts')]
  --         Fix (Term'Record name fields) -> do
  --           name' <- evalSym args1' name
  --           let (keys, vals) = unzip (M.toList fields)
  --           keys' <- mapM (evalSym args1') keys
  --           vals' <- mapM (app >=> extractSingleton) vals
  --           let fields' = M.fromList (zip keys' vals')
  --           return [Fix (Term'Record name' fields')]
  --         Fix (Term'Abs ps ctx body) -> do
  --           ps' <- mapM (evalSym args1') ps
  --           body' <- concat `fmap` mapM app body
  --           return [Fix (Term'Abs ps' ctx body')]
  --         Fix (Term'Fiber ps ctx body) -> do
  --           ps' <- mapM (evalSym args1') ps
  --           body' <- concat `fmap` mapM app body
  --           return [Fix (Term'Fiber ps' ctx body')]
  --         Fix (Term'Yield t) -> do
  --           t' <- (app >=> extractSingleton) t
  --           return [Fix (Term'Yield t')]
  --         Fix (Term'Resume name args) -> do
  --           name' <- evalSym args1' name
  --           args' <- concat `fmap` mapM app args
  --           return [Fix (Term'Resume name' args')]
  --         Fix (Term'AppAbs abs args) -> do
  --           abs'  <- (app >=> extractSingleton) abs
  --           args' <- concat `fmap` mapM app args
  --           return [Fix (Term'AppAbs abs' args')]
  --         Fix (Term'Match t cases) -> undefined -- TODO
  --         Fix (Term'MacroSplice name) ->
  --           case lookup name args1' of
  --             Just arg -> return (syntaxToTerm arg)
  --             Nothing  -> preprocError ("Unknown macro parameter `" ++ name ++ "'")
  --         _ -> return [term]
  --       appTy ty = case ty of
  --         Type'Placeholder name -> do
  --           name' <- evalSym args1' name
  --           return (Type'Placeholder name')
  --         Type'TypeVar name -> do
  --           name' <- (Just `fmap` evalSym args1' name) `catchError` const (return Nothing)
  --           case name' of
  --             Just sym -> return [Fix (Term'Var sym)]
  --             -- if it's not a valid symbol splice, it must be a term splice
  --             Nothing -> appTy (Fix (Term'MacroSplice (tail name)))
  --           name' <- evalSym args1' name
  --           return (Type'TypeVar name')
  --         Type'TypeAbs ps ret -> do
  --           ps' <- mapM (evalSym args1') ps
  --           ret' <- appTy ret
  --           return (Type'TypeAbs ps' ret')
  --         Type'AppTypeAbs abs args -> do
  --           abs' <- appTy abs
  --           args' <- mapM appTy args
  --           return (Type'AppTypeAbs abs' args')
  --         Type'Product ts -> do
  --           ts' <- mapM appTy ts
  --           return (Type'Product ts')
  --         Type'List ty -> do
  --           ty' <- appTy ty
  --           return (Type'List ty')
  --         Type'Record name fields -> do
  --           name' <- evalSym args1' name
  --           let (fnames, fvals) = unzip (M.toList fields)
  --           fnames' <- mapM (evalSym args1') fnames
  --           fvals' <- mapM appTy fvals
  --           let fields' = M.fromList (zip fnames' fvals')
  --           return (Type'Record name' fields')
  --         Type'Abs ps ret -> do
  --           ps' <- mapM appTy ps
  --           ret' <- appTy ret
  --           return (Type'Abs ps' ret')
  --         Type'Fiber ps ret -> do
  --           ps' <- mapM appTy ps
  --           ret' <- appTy ret
  --           return (Type'Fiber ps' ret')
  --         Type'Variant name fields -> do
  --           name' <- evalSym args1' name
  --           let (fnames, fvals) = unzip (M.toList fields)
  --           fnames' <- mapM (evalSym args1') fnames
  --           fvals' <- mapM (mapM appTy) fvals
  --           let fields' = M.fromList (zip fnames' fvals')
  --           return (Type'Variant name' fields')
  --         _ -> return ty

syntaxToTerm :: SyntaxElement -> [Term]
syntaxToTerm (Syntax'Decl t) = [t]
syntaxToTerm (Syntax'Term t) = [t]
syntaxToTerm (Syntax'Val v)  = [v]
syntaxToTerm (Syntax'List ts) = ts >>= syntaxToTerm
syntaxToTerm (Syntax'Type ty) = undefined
syntaxToTerm (Syntax'Name name) = undefined

-- | Evaluates a symbol. If the second argument begins with a tilde (~),
-- this function will try to macroexpand it into another symbol by looking it up from the symbol alist;
-- otherwise, it will be returned unchanged.
evalSym :: [(Symbol, SyntaxElement)] -> Symbol -> MushroomPreproc Symbol
evalSym symMap ('~':sym) =
  case lookup sym symMap of
    Just (Syntax'Name name) -> return name
    _ -> preprocError ("The macro parameter `" ++ sym ++ "' should be a symbol, but is not")
evalSym symMap sym = return sym

-- | Extracts a term from a singleton list. If the list does not have exactly one element, this
-- function throws an error. It should be used for processing the results of a macroexpansion
-- in a context where only one term is syntactically valid.
extractSingleton :: [Term] -> MushroomPreproc Term
extractSingleton [t] = return t
extractSingleton _   = preprocError "Expected one term in macroexpansion, but got multiple"

preprocError :: String -> MushroomPreproc a
preprocError = throwError . newMushroomError PreprocError
