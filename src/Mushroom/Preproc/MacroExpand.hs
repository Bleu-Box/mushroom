module Mushroom.Preproc.MacroExpand where

import Control.Lens
import Control.Monad ((>=>), liftM)
import Control.Monad.Except
import Control.Monad.State

import Data.Fix
import Data.List (mapAccumL)
import Data.Maybe
import Data.Tuple (swap)

import Mushroom.PrettyPrint
import Mushroom.Preproc.Util
import Mushroom.Types

runPreprocessor :: PreprocState -> [Term] -> Either MushroomError (PreprocState, [Term])
runPreprocessor pState terms =
  case sequence (snd results) of
    Left msg -> Left msg
    Right terms' -> Right (fst results, filter (not . isMacroDecl) (concat terms'))
  where results = mapAccumL go pState terms
        go c t = swap $ runState (runExceptT . runPreproc $ macroExpand t) c

-- | Evaluates macro applications by expanding them into a list of terms.
macroExpand :: Term -> MushroomPreproc [Term]
macroExpand Fix{unFix = Term'MacroDecl name ps1 ps2 body} = do
  body' <- concat `fmap` mapM macroExpand body
  addMacro name (ps1, ps2, body')
  return [Fix (Term'MacroDecl name ps1 ps2 body')]

macroExpand Fix{unFix = Term'Pin t} = do
  t' <- (macroExpand >=> extractSingleton) t
  return [Fix (Term'Pin t')]

macroExpand Fix{unFix = Term'Define name t} = do
  t' <- (macroExpand >=> extractSingleton) t
  return [Fix (Term'Define name t')]

macroExpand Fix{unFix = Term'ModuleDecl _ _ _ _} = undefined

macroExpand Fix{unFix = Term'List ts} = do
  ts' <- mapM (macroExpand >=> extractSingleton) ts
  return [Fix (Term'List ts')]

macroExpand Fix{unFix = Term'Product ts} = do
  ts' <- mapM (macroExpand >=> extractSingleton) ts
  return [Fix (Term'Product ts')]

macroExpand Fix{unFix = Term'Variant name args} = do
  args' <- mapM (macroExpand >=> extractSingleton) args
  return [Fix (Term'Variant name args')]

macroExpand Fix{unFix = Term'Record name fields} = do
  fields' <- mapM (macroExpand >=> extractSingleton) fields
  return [Fix (Term'Record name fields')]

macroExpand Fix{unFix = Term'Abs ps ctx body} = do
  body' <- concat `fmap` mapM macroExpand body
  return [Fix (Term'Abs ps ctx body')]

macroExpand Fix{unFix = Term'Fiber ps ctx body} = do
  body' <- concat `fmap` mapM macroExpand body
  return [Fix (Term'Fiber ps ctx body')]

macroExpand Fix{unFix = Term'Yield t} = do
  t' <- (macroExpand >=> extractSingleton) t
  return [Fix (Term'Yield t')]

macroExpand Fix{unFix = Term'Resume name args} = do
  args' <- mapM (macroExpand >=> extractSingleton) args
  return [Fix (Term'Resume name args')]

macroExpand Fix{unFix = Term'AppAbs abs args} = do
  abs' <- (macroExpand >=> extractSingleton) abs
  args' <- mapM (macroExpand >=> extractSingleton) args
  return [Fix (Term'AppAbs abs' args')]

-- macroExpand Fix{unFix = Term'Match t cases} = do
--   t' <- macroExpand t
--   cases' <- mapM (mapM (mapM macroExpand)) cases
--   return $ Fix (Term'Match t', cases')

macroExpand Fix{unFix = Term'AppMacro name args1 args2} = do
  -- get the info on this macro
  mac@(ps1, ps2, body) <- lookupMacro name

  -- `typecheck' the first arguments to the macro
  checkArgs args1 ps1
  -- if the second arguments are there, check them too
  if isJust args2
    then checkArgs [fromJust args2] [fromJust ps2]
    else return ()

  -- "apply" the macro via syntactic replacement and traversal
  applyMacro mac args1 args2

macroExpand t = return [t]
