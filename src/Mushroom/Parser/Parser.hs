module Mushroom.Parser.Parser (parseMushroom) where

import Text.Parsec

import Mushroom.Types
import Mushroom.Parser.Util
import Mushroom.Parser.Terms

parseMushroom :: String -> Either ParseError ([FilePath], [Term])
parseMushroom = runParser mushroomParser ParseState{inMacro=False} ""

mushroomParser :: Parser ([FilePath], [Term])
mushroomParser = do
  fpaths <- require `sepBy` semi
  terms <- term `sepEndBy` semi
  eof
  return (fpaths, terms)

require :: Parser FilePath
require = reserved "require" >> str
