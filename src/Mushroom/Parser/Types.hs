module Mushroom.Parser.Types (typeLiteral, variantTypeTag) where

import Control.Monad (liftM)
import Prelude hiding (abs, product)

import Text.Parsec
import Text.Parsec.Char

import Mushroom.Types
import Mushroom.Parser.Util

-- | Parses a type literal as a type.
typeLiteral :: Parser Type
typeLiteral =
  try typeAbs
  <|> try typeFiber
  <|> typeProduct
  <|> typeList
  <|> typeSet
  <|> try typeAppTypeAbs
  <|> typeName

-- | Parses a type name, e.g. `int', `str', `my_custom_type'.
typeName :: Parser Type
typeName = do
  ty <- try typePlaceholderName <|> qualifiedIdent
  return $ if ty `elem` builtinTypes then readBuiltinType ty
           else if head ty == '\'' then Type'Placeholder ty
                else Type'TypeVar ty
  where builtinTypes = ["int", "float", "str", "self", "char"]
        readBuiltinType "int" = Type'Int
        readBuiltinType "float" = Type'Float
        readBuiltinType "str" = Type'Str
        readBuiltinType "self" = Type'Self
        readBuiltinType "char" = Type'Char

-- | Parses a type abstraction type, e.g.:
-- > maybe<int>
typeAppTypeAbs :: Parser Type
typeAppTypeAbs = do
  abs <- typeName
  args <- angles (commaSep typeLiteral)
  return $ Type'AppTypeAbs abs args

-- | Parses the name of a type placeholder, which is a normal identifier preceded by a tick mark:
-- > 'a
typePlaceholderName :: Parser String
typePlaceholderName = do
  char '\''
  name <- liftM ('\'':) ident
  return name

-- | Parses a product type, e.g.:
-- > <int, int, str>
typeProduct :: Parser Type
typeProduct = liftM Type'Product (angles $ commaSep typeLiteral)

-- | Parses a list type:
-- > [int]
typeList :: Parser Type
typeList = liftM Type'List (brackets typeLiteral)

-- | Parses a set type, e.g.:
-- > #{int}
typeSet :: Parser Type
typeSet = do
  char '#'
  ty <- braces typeLiteral
  return (Type'Set ty)

-- | Parses an abstraction type, e.g.:
-- > (str, str) -> <>
typeAbs :: Parser Type
typeAbs = do
  params <- parens (commaSep typeLiteral)
  arrow
  ret <- typeLiteral
  return $ Type'Abs params ret

-- | Parses a fiber type, e.g.:
-- > (str, str) --> <>
typeFiber :: Parser Type
typeFiber = do
  params <- parens (commaSep typeLiteral)
  longArrow
  ret <- typeLiteral
  return $ Type'Fiber params ret

-- | Parses the type of a variant tag, e.g.:
-- > #just<'a>
variantTypeTag :: Parser (Symbol, [Type])
variantTypeTag = do
  char '#'
  tagName <- ident
  paramTypes <- option [] $ angles (commaSep typeLiteral)
  return ('#':tagName, paramTypes)
