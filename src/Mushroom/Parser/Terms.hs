module Mushroom.Parser.Terms (term) where

import Prelude hiding (abs, product)

import Control.Monad (liftM)

import Data.Fix
import Data.Functor.Identity (Identity)
import qualified Data.Map as M
import Data.Text (pack)

import Debug.Trace (trace)

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.Expr

import Mushroom.Types
import Mushroom.Parser.Types
import Mushroom.Parser.Util

term :: Parser Term
term = decl <|> value

decl :: Parser Term
decl =
  appMacro
  <|> try moduleDecl
  <|> macroDecl
  <|> letDecl
  <|> varDecl
  <|> rcdDecl
  <|> try fiberDecl
  <|> fnDecl

-- | Like a @decl@, but only contains declarations that are valid in local contexts (like functions). 
localDecl :: Parser Term
localDecl = letDecl <|> try fiberDecl <|> fnDecl

moduleDecl :: Parser Term
moduleDecl = do
  reserved "mod"
  name <- ident
  termExports <- params
  typeExports <- params
  decls <- braces (decl `sepEndBy` semi)
  retFixed $ Term'ModuleDecl name termExports typeExports decls

macroDecl :: Parser Term
macroDecl = do
  reserved "macro"
  name <- ident
  params1 <- try macroParams <|> return []
  params2 <- try (braces listParam) <|> return Nothing
  -- enable macro-style identifiers in the body
  enterMacro
  body <- braces $ semiSep1 term
  exitMacro
  retFixed $ Term'MacroDecl name params1 params2 body
  where listParam = do
          name <- ident
          colon
          t <- tySyntaxList
          return (Just (name, t))
    
letDecl :: Parser Term
letDecl = do
  reserved "let"
  name <- ident
  equals
  val <- value
  retFixed $ Term'Define name val

fiberDecl :: Parser Term
fiberDecl = do
  reserved "fiber"
  name <- ident
  ps <- params
  body <- braces $ semiSep1 (try localDecl <|> try yield <|> value)
  let fiber = Fix $ Term'Fiber ps M.empty body
  retFixed $ Term'Define name fiber

fnDecl :: Parser Term
fnDecl = do
  reserved "fn"
  name <- ident
  ps <- params
  body <- braces $ semiSep1 (try localDecl <|> value)
  let abs = Fix $ Term'Abs ps M.empty body
  retFixed $ Term'Define name abs

varDecl :: Parser Term
varDecl = do
  reserved "variant"
  name <- ident
  tags <- braces (variantTypeTag `sepEndBy1` comma)
  retFixed $ Term'VarDecl name tags

rcdDecl :: Parser Term
rcdDecl = do
  reserved "record"
  name <- ident
  fields <- braces (field `sepBy1` comma)
  retFixed $ Term'RcdDecl name fields
  where field = do
          fname <- ident
          colon
          ty <- typeLiteral
          return (fname, ty)

value :: Parser Term
value =
  try appOp
  <|> try application
  <|> try appMacro
  <|> try variant
  <|> try set
  <|> try macroSpliceList
  <|> try macroSplice
  <|> try record
  <|> try firstClassOp
  <|> try (parens value)
  <|> try match
  <|> try abs
  <|> try resume
  <|> list
  <|> product
  <|> floatLit
  <|> intLit
  <|> strLit
  <|> charLit
  <|> liftMFixed Term'Var ident

macroSplice :: Parser Term
macroSplice = do
  symbol "~"
  name <- ident
  retFixed (Term'MacroSplice name)

macroSpliceList :: Parser Term
macroSpliceList = do
  symbol "~,"
  name <- ident
  retFixed (Term'MacroSpliceList name)

yield :: Parser Term
yield = do
  reserved "yield"
  val <- value
  retFixed $ Term'Yield val

resume :: Parser Term
resume = do
  reserved "resume"
  fiberName <- ident
  args <- parens (commaSep value)
  retFixed $ Term'Resume fiberName args

record :: Parser Term
record = do
  symbol "%"
  name <- ident
  fields <- braces (field `sepBy1` comma)
  retFixed $ Term'Record name (M.fromList fields)
  where field = do
          fname <- ident
          colon
          val <- value
          return (fname, val)

product :: Parser Term
product = liftMFixed Term'Product $ angles (commaSep value)

list :: Parser Term
list = liftMFixed Term'List $ brackets (commaSep value)

-- | A builtin operator, but treated like a variable
-- (like `(+)` vs `1 + 2` in Haskell)
firstClassOp :: Parser Term
firstClassOp = do
  sym <- parens (oneOf "+-*/")
  retFixed $ Term'Builtin (symToBuiltin [sym])

appOp :: Parser Term
appOp = buildExpressionParser ops operand
  where operand = try (parens appOp)
                  <|> try (parens match)
                  <|> try application
                  <|> try floatLit
                  <|> try intLit
                  <|> variable

application :: Parser Term
application = try normalApp <|> openApp
  where varOrBuiltin = qualifiedIdent >>= \name ->
          if name `elem` builtinSyms
          then retFixed (Term'Builtin (symToBuiltin name))
          else retFixed (Term'Var name)
        normalApp = do
          f <- fn
          args <- parens (commaSep value)
          retFixed $ Term'AppAbs f args
        openApp = backticks $ do
          f <- fn
          args <- many value
          retFixed $ Term'AppAbs f args
        fn = try abs
             <|> try openApp
             <|> try (parens normalApp)
             <|> varOrBuiltin
  
variant :: Parser Term
variant = do
  char '#'
  name <- qualifiedIdent
  args <- angles (commaSep value) <|> return []
  retFixed $ Term'Variant ('#':name) args

set :: Parser Term
set = do
  char '#'
  elems <- braces (commaSep value)
  retFixed $ Term'Set elems

abs :: Parser Term
abs = braces absBody
  where absBody = do
          ps <- between (symbol "|") (symbol "|") (commaSep ident)
          body <- semiSep1 (try letDecl <|> value)
          retFixed $ Term'Abs ps M.empty body

match :: Parser Term
match = do
  reserved "match"
  val <- value
  branches <- braces (matchBranch `sepEndBy1` comma)
  retFixed $ Term'Match val branches

matchBranch :: Parser (Term, [Term])
matchBranch = do
  matchVal <- try pin
             <|> try value -- patterns can be a value, a pin, or wildcard
             <|> liftMFixed Term'Var (symbol "_")
  thickArrow
  result <- try longResult <|> (value >>= \v -> return [v])
  return (matchVal, result)
  where longResult = braces ((try letDecl <|> try fnDecl <|> value) `sepBy1` semi)

appMacro :: Parser Term
appMacro = do
  symbol "@"
  name <- ident
  args1 <- try parenArgs <|> return []
  args2 <- try (liftM Just braceArgs) <|> return Nothing
  retFixed $ Term'AppMacro name args1 args2
  where parenArgs = parens (commaSep syntaxElem)
        braceArgs = syntaxList

-- | Parse a pinned value (used in match branches). Pinned values are actual values that are meant to
-- be evaluated and /then/ matched against, and they begin with a ^.
pin :: Parser Term
pin = do
  char '^'
  val <- value
  retFixed $ Term'Pin val

variable, strLit, intLit :: Parser Term
variable = liftMFixed Term'Var qualifiedIdent
strLit = liftMFixed (Term'StrLit . pack) str
intLit = liftMFixed Term'IntLit int
floatLit = liftMFixed Term'FloatLit float
charLit = liftMFixed Term'CharLit $ between (char '\'') (char '\'') anyChar

-- | Creates an infix builtin function given a symbol.
ibuiltin s = reservedOp s >> return f
  where f :: Term -> Term -> Term
        f x y = Fix $ Term'AppAbs (Fix $ Term'Builtin (symToBuiltin s)) [x, y]

-- | The table of parsers for the infix operators the language uses.
ops :: [[Operator String u Identity Term]]
ops = [ [Infix (ibuiltin "+") AssocLeft]
      , [Infix (ibuiltin "-") AssocLeft]
      , [Infix (ibuiltin "*") AssocLeft]
      , [Infix (ibuiltin "/") AssocLeft] ]

params :: Parser [Symbol]
params = parens (commaSep ident)

macroParams :: Parser [(Symbol, SyntaxType)]
macroParams = parens (commaSep mparam)
  where mparam = do
          name <- ident
          colon
          ty <- syntaxType
          return (name, ty)

syntaxType :: Parser SyntaxType
syntaxType = tyDecl <|> try tyType <|> tyTerm <|> tyVal <|> tyName
  where tyDecl = reserved "decl" >> return SyntaxTy'Decl
        tyType = reserved "type" >> return SyntaxTy'Type
        tyTerm = reserved "term" >> return SyntaxTy'Term
        tyVal  = reserved "val"  >> return SyntaxTy'Val
        tyName = reserved "name" >> return SyntaxTy'Name

tySyntaxList :: Parser SyntaxType
tySyntaxList = symbol "&" >> liftM SyntaxTy'List syntaxType

syntaxElem :: Parser SyntaxElement
syntaxElem = try sdecl
             <|> sval
             <|> try sterm
             <|> try stype
             <|> try sname
  where sdecl = liftM Syntax'Decl decl
        sterm = liftM Syntax'Term term
        sval  = liftM Syntax'Val value
        stype = liftM Syntax'Type typeLiteral
        sname = char ':' >> liftM Syntax'Name ident

syntaxList :: Parser SyntaxElement
syntaxList = liftM Syntax'List (braces $ try (commaSep syntaxElem) <|> semiSep syntaxElem)
