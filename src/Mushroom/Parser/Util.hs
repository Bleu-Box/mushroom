{-# LANGUAGE ExistentialQuantification, TypeApplications #-}

module Mushroom.Parser.Util where

import Control.Monad (liftM)

import Data.Functor.Identity (Identity)
import Data.List (intercalate)
import Data.Fix

import GHC.Float (double2Float)

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.Language
import qualified Text.Parsec.Token as T

newtype ParseState = ParseState { inMacro :: Bool }

type Parser = Parsec String ParseState

builtinSyms :: [String]
builtinSyms = ["hd", "tl", "print", "cons", "strToList", "listToStr"]

mushroomDef :: (Monad m) => GenLanguageDef String u m
mushroomDef = T.LanguageDef
  { T.commentStart = "/*"
  , T.commentEnd = "*/"
  , T.commentLine = "//"
  , T.nestedComments = True
  , T.identStart = letter
  , T.identLetter = alphaNum <|> char '_'
  , T.opStart = oneOf "+/*-"
  , T.opLetter = oneOf "+/*-"
  , T.reservedNames = [ "match", "let", "fn", "variant", "require", "record"
                      , "fiber", "yield", "resume" {-, "mod", "macro", "decl"
                      , "term", "val", "type", "name"-} ]
  , T.reservedOpNames = ["+", "-", "/", "*"]
  , T.caseSensitive = True }

lexer :: (Monad m) => T.GenTokenParser String st m
lexer = T.makeTokenParser mushroomDef

enterMacro, exitMacro :: Parser ()
enterMacro = modifyState (\st -> st{inMacro = True})
exitMacro  = modifyState (\st -> st{inMacro = False})

-- | Parses an identifier with qualifiers.
qualifiedIdent :: Parser String
qualifiedIdent = do
  names <- ident `sepBy1` string "::"
  return (intercalate "::" names)

-- | Parses an identifier. If the identifier is inside a macro, it can optionally have an octothorpe (#) at the
-- end. Since such identifiers result in syntax errors outside of macros, using them can guarantee macro hygiene
-- by preventing the capture unwanted outside variables.
ident :: Parser String
ident = do
  -- This is a gruesome kludge, but hopefully I'll remove it later.
  -- Rather than double the term datatype's number of tags, some symbols can
  -- be prefixed with tildes, which *sigh* will be interpreted as macroexpansions
  -- and removed after the preprocessor step.
  tilde <- try (symbol "~") <|> return ""
  name <- ident'
  st <- getState
  octo <- if inMacro st then try (symbol "#") <|> return ""
          else return ""
  return (tilde ++ name ++ octo)
  where ident' :: ParsecT String u Identity String
        ident' = T.identifier lexer

reserved :: String -> ParsecT String u Identity ()
reserved = T.reserved lexer

reservedOp :: String -> ParsecT String u Identity ()
reservedOp = T.reservedOp lexer

parens :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m a
parens = T.parens lexer

angles :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m a
angles = T.angles lexer

brackets :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m a
brackets = T.brackets lexer

braces :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m a
braces = T.braces lexer

backticks :: ParsecT String u Identity a -> ParsecT String u Identity a
backticks = between (symbol "`") (symbol "`")

int :: ParsecT String u Identity Int
int = fromIntegral @Integer @Int `fmap` T.integer lexer

float :: ParsecT String u Identity Float
float = double2Float `fmap` T.float lexer

whitespace :: ParsecT String u Identity ()
whitespace = T.whiteSpace lexer

str :: ParsecT String u Identity String
str = T.stringLiteral lexer

colon :: ParsecT String u Identity String
colon = T.colon lexer

semi :: ParsecT String u Identity String
semi = T.semi lexer

dot :: ParsecT String u Identity String
dot = T.dot lexer

comma :: ParsecT String u Identity String
comma = T.comma lexer

commaSep :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m [a]
commaSep = T.commaSep lexer

semiSep :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m [a]
semiSep = T.semiSep lexer

semiSep1 :: forall u m a. (Monad m) => ParsecT String u m a -> ParsecT String u m [a]
semiSep1 = T.semiSep1 lexer

symbol :: String -> ParsecT String u Identity String
symbol = T.symbol lexer

equals :: ParsecT String u Identity String
equals = symbol "="

arrow :: ParsecT String u Identity String
arrow = symbol "->"

thickArrow :: ParsecT String u Identity String
thickArrow = symbol "=>"

longArrow :: ParsecT String u Identity String
longArrow = symbol "-->"

-- | Like @retFixed@, but retFixeds a @Fix@ed value.
retFixed x = return (Fix x)

-- | Like @liftM@, but retFixeds a @Fix@ed value.
liftMFixed f = liftM (Fix . f)
