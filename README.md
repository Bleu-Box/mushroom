# Update
This was an interesting project, but I stopped development after too much cruft accumulated and life became too busy to continue. There's a lot of spaghetti code, since I made this in high school and didn't have a good idea of how to structure programs. However, the typechecker is still pretty good.

There were plans for an LLVM compiler, but those have been shelved. You can find the remnants of that plan stored in the `compiler` branch. There's also a branch called `own-vm`, from when I tried to compile mushroom to a set of custom bytecode instructions. All in all, it was a nice learning experience.

# Mushroom
Mushroom is a programming language that I'm designing. It's still in its preliminary stages, so it's incomplete and the edge cases aren't fully worked out.
You can now find a profile of it on [proglangdesign.net](https://proglangdesign.net/). See the [wiki](https://gitlab.com/Bleu-Box/mushroom/wikis/home) to find a tour of the language's features.

## Development Status
_See the [checklist](#checklist) for what's been completed._
Most of the bare minimum language features have been implemented, and it can be run through an interpreter. Currently I'm working on putting together an LLVM compiler for it. Sometime during this process I'd also like
to finish implementing the module system. A standard library, macros, optimizations, and FFI may be implemented in the future.

## Goals
### Type System
Mushroom uses System F with things like product types, lists, records, variant types, anonymous functions, universal types, recursive types, and record types.
There's no plans for existential types yet, but simple typeclass-like behavior can still be faked using record types (I'll probably make a tutorial for the language sometime that includes this).
Type inference is accomplished via unification.

## Execution
As of right now, Mushroom runs through an interpreter.
In the end, I'd like to have this be a compiled language that compiles down to LLVM bytecodes or custom bytecodes
(i.e., it would use in some type of bytecode VM).

### Concurrency
Mushroom accomplishes concurrency via fibers.

### Macros
Mushroom's syntax is extensible via basic macro system (currently in progress).

### Purpose
Mushroom's end purpose is to be used as a language for communication, like Erlang but with a better type system
and macros.
 
## Syntax
The most aesthetically similar language to Mushroom is probably Rust. However, Mushroom has a few more
fun symbols and sigils.

## Usage (if you use Nix)
```bash
# build the project
nix-build release.nix
# repl
./result/bin/mushroom repl
# read from file
./result/bin/mushroom run /path/to/file.shr
```

## Usage (otherwise)
```bash
# To run from a file
stack exec -- mushroom run path/to/file.shr
# To use the REPL
stack exec -- mushroom repl
```

## Checklist
- [x] int
- [x] str
- [x] Products
- [x] Lists
- [x] Variants
- [x] Abstractions
- [x] Pattern matching
- [x] Universal polymorphism
- [x] Recursive types
- [x] Recursive functions
- [ ] Compiler
- [ ] Modules
- [ ] stdlib
- [x] char, float
- [x] records
- [x] fibers
- [ ] macros

## Resources
I'm using a few very helpful resources to make this language, primarily:
- _Types and Programming Languages_, by Benjamin Pierce
- _Crafting a Compiler_, by Charles N. Fischer, Ron K. Cytron, and Richard J. LeBlanc, Jr.
- Stephen Diehl's [Hindley-Milner](http://dev.stephendiehl.com/fun/006_hindley_milner.html) and [LLVM](http://www.stephendiehl.com/llvm/) tutorials
